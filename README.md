# OpenML dataset: Condominium-Comparable-Rental-Income-in-NYC

https://www.openml.org/d/43361

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

If you think the dataset is useful please vote for it, it's an assignment from my data science class, I'll be appreciate! :))
Context
The Department of Finance (DOF) is required by NY State law to value condominiums or cooperatives as if they were residential rental apartment buildings. DOF uses income information from rental properties similar in physical features and location to the condominiums or cooperatives. DOF applies this income data to the condominium or cooperative to determine its value in the same way DOF values rental apartment buildings. This report includes information at a condominium suffix level which represents a subdivision of the condominium since DOF values condominiums at a suffix level. A condominium may have more than one suffix.
Content
This data set contains the reports from 2012-2018.
Boro-Block-Lot    
The Borough-Block-Lot location of the subject condominium. The lot identifies the condominium billing lot generally associated with the condominium management organization.
Address
The Street Address of the property
Neighborhood    
Department of Finance determines the neighborhood name in the course of valuing properties. The common name of the neighborhood is generally the same as the name Finance designates. However, there may be slight differences in neighborhood boundary lines.
Building Classification    
The Building Class code is used to describe a propertys use. This report includes the two character code as well as the description of the building class.
Total Units
Total number of units in the building
Year Built    
The year the building was built
Gross SqFt    
Gross square footage of the building
Estimated Gross Income    
Estimated Income per SquareFoot * Gross SquareFoot
Estimated Expense
Estimated Expense per SquareFoot * Gross SquareFoot
Net Operating Income
Estimated Gross Income-Estimated Expense
Full Market Value
Current years total market value of the land and building
Report Year
Acknowledgements
Agency: Department of Finance (DOF)
Source: NYC open data

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43361) of an [OpenML dataset](https://www.openml.org/d/43361). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43361/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43361/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43361/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

